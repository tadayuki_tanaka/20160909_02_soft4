■コミットに失敗した原因の検証実施

◆gitコマンド（git add）実行エラー時の、原因再検証１
　→原因１：ワークスペースを作るとき"Clone from Git or Mercurial URL (optional)"の入力誤り
　　　正解：git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git
　　　誤り：git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4
　　　　　　※｢.git｣を付加しなかった

【再現テスト結果】

ワークスペースを作るとき"Clone from Git or Mercurial URL (optional)"の入力時、
間違って｢git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4｣と入力する
※｢.git｣を付加しない

soft4_t:~/workspace $ cd /home/ubuntu/workspace

soft4_t:~/workspace $ echo "眠い" >> test_0909_03.php

soft4_t:~/workspace $ git add test_0909_03.php
★「git add」で失敗する → 「.git」付加なしが原因！
fatal: Not a git repository (or any of the parent directories): .git

soft4_t:~/workspace $ git remote -v
fatal: Not a git repository (or any of the parent directories): .git

soft4_t:~/workspace $ git remote add origin git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git
fatal: Not a git repository (or any of the parent directories): .git

soft4_t:~/workspace $ git init
Initialized empty Git repository in /home/ubuntu/workspace/.git/

★repositoryのremote設定を再度実施
soft4_t:~/workspace (master) $ git remote add origin git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git

soft4_t:~/workspace (master) $ git remote -v
origin  git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git (fetch)
origin  git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git (push)

soft4_t:~/workspace (master) $ git add test_0909_03.php
★「git add」がうまくいった

soft4_t:~/workspace (master) $ git commit -m '更新内容_0909_03'
[master (root-commit) 25173a6] 更新内容_0909_03
 1 file changed, 1 insertion(+)
 create mode 100644 test_0909_03.php

soft4_t:~/workspace (master) $ git push -u origin master
Warning: Permanently added 'bitbucket.org,104.192.143.2' (RSA) to the list of known hosts.
★前回でていなかったWarningが発生している
Counting objects: 3, done.
Writing objects: 100% (3/3), 248 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
★「git push」がうまくいった

soft4_t:~/workspace (master) $ 
　
　
　
　
　
◆gitコマンド（git add）実行エラー時の、原因再検証２
　→原因２：workspaceフォルダ配下の「.git」フォルダを誤って削除

【再現テスト結果】

workspaceフォルダ配下の「git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git」フォルダを削除

soft4_t:~/workspace (master) $ cd /home/ubuntu/workspace

soft4_t:~/workspace (master) $ echo "眠い" >> test_0909_03.php

soft4_t:~/workspace (master) $ git add test_0909_03.php
★「git add」はうまくいく → 「.git」フォルダ削除が原因ではない

soft4_t:~/workspace (master) $ git commit -m '更新内容_0909-03'
[master c110c5f] 更新内容_0909-03
 1 file changed, 1 insertion(+)

soft4_t:~/workspace (master) $ git push -u origin master
★この場合「git push」が失敗する
Warning: Permanently added 'bitbucket.org,104.192.143.3' (RSA) to the list of known hosts.
conq: repository does not exist.
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

soft4_t:~/workspace (master) $ git remote -v
origin  git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git (fetch)
origin  git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git (push)

soft4_t:~/workspace (master) $ 
　
　
　
　
　
◆gitコマンド（git add）実行エラー時の、原因再検証３
　→原因３：ワークスペースを作るとき"Clone from Git or Mercurial URL (optional)"の入力誤り
　　　正解：git@bitbucket.org:tadayuki_tanaka/20160909_2_soft4.git
　　　誤り：git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git
　　　　　　※｢:｣と｢/｣を間違えた

【再現テスト結果】

ワークスペースを作るとき"Clone from Git or Mercurial URL (optional)"の入力時、
間違って｢git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git｣と入力する
※｢:｣と｢/｣を間違えて入力

soft4_t:~/workspace (master) $ cd /home/ubuntu/workspace

soft4_t:~/workspace (master) $ echo "眠い" >> test_0909-2.php

soft4_t:~/workspace (master) $ git add test_0909-2.php
★「git add」はうまくいく

soft4_t:~/workspace (master) $ git commit -m '更新内容_0909-2'
★「git commit」もうまくいく
[master (root-commit) 9ff0826] 更新内容_0909-2
 1 file changed, 1 insertion(+)
 create mode 100644 test_0909-2.php

soft4_t:~/workspace (master) $ git push -u origin master
★この場合「git push」が失敗する
fatal: 'git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

soft4_t:~/workspace (master) $ git remote -v
origin  git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git (fetch)
origin  git@bitbucket.org/tadayuki_tanaka/20160909_2_soft4.git (push)

soft4_t:~/workspace (master) $
　
　
　
　
　
以上
